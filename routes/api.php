<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('todo',\App\Http\Controllers\Api\V1\TodoController::class)->except('store');
Route::post('todo',[\App\Http\Controllers\Api\V1\TodoController::class,'create']);
Route::put('todo/{todo}/change',[\App\Http\Controllers\Api\V1\TodoController::class,'change'])->name('todo.change');
