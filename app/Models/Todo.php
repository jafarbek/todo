<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    const STATUS_NEW = 'new';
    const STATUS_DOING = 'doing';
    const STATUS_DONE = 'done';
    const STATUS_CANCEL = 'cancel';

    use HasFactory;
    protected $table = 'todos';
    protected $fillable = ['name', 'status'];

    public static function statusesList()
    {
        return [
            self::STATUS_NEW,
            self::STATUS_DOING,
            self::STATUS_DONE,
            self::STATUS_CANCEL,
        ];
    }

    public function getAcceptedStatuses(): array
    {
        switch ($this->status){
            case  self::STATUS_NEW: return [self::STATUS_DOING,self::STATUS_DONE,self::STATUS_CANCEL];
            case  self::STATUS_DOING: return [self::STATUS_DONE,self::STATUS_CANCEL];
            case  self::STATUS_DONE:
            case  self::STATUS_CANCEL: return [];
        }
    }
}
