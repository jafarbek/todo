<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChangeStatusTodoRequest;
use App\Http\Requests\TodoEditRequest;
use App\Http\Resources\TodoResource;
use App\Models\Todo;
use http\Client\Response;
use Illuminate\Auth\Events\Validated;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Request;
use function Symfony\Component\Translation\t;

class TodoController extends Controller
{
    public function index()
    {
        return TodoResource::collection(Todo::paginate(20));
    }

    public function create(TodoEditRequest $request)
    {
        $todo = Todo::create($request->validated());
        return new TodoResource($todo);
    }

    public function show($id)
    {
        $todo = Todo::find($id);
        if (!$todo) {
            return response()->json('there is no such todo in the system');
        }
        return new TodoResource($todo);
    }

    public function update(TodoEditRequest $request, $id)
    {
        $todo = Todo::find($id);
        if (!$todo) {
            return response()->json('there is no such todo in the system');
        }
        $todo->update($request->validated());
        return new TodoResource($todo);
    }

    public function change(ChangeStatusTodoRequest $request, Todo $todo)
    {
        if (!$todo) {
            return response()->json('there is no such todo in the system');
        }
        $todo->update($request->validated());
        return new TodoResource($todo);
    }

    public function destroy($id)
    {
        $todo = Todo::find($id);
        if (!$todo) {
            return response()->json('there is no such todo in the system');
        }
        if ($todo->delete()) {
            return response()->json('successfully deleted');
        }
    }
}
